## Useful bookmarklets for MAM

- Notifications: Enable / Disable shoutbox notifications for all messages.

## Usage
Head over to [this web page](https://d3vr.gitlab.io/mam), and grab the bookmarklets to your bookmarks bar.