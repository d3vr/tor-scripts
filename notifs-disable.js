// Re-enable default behavior for shoutbox to stop loading messages when window is not focused
hiddenProperty = "hidden" in document ? "hidden" : "webkitHidden" in document ? "webkitHidden" : "mozHidden" in document ? "mozHidden" : null;
if (hiddenProperty != null) {
    var visibilityChangeEvent = hiddenProperty.replace(/hidden/i, "visibilitychange")
} else {
    var visibilityChangeEvent = null
}
document.addEventListener(visibilityChangeEvent, shoutboxOnVisibilityChange)

// Disable
if(typeof window.ping_all !== "undefined"){
    window.ping_all = false;
    console.info("[NOTIFICATIONS]: deactivated");
    $("#notif_state").css("background-color", "#AAA")

}else{
    console.warn("[NOTIFICATIONS]: not activated yet");
}