// Disable shoutbox from not loading messages
visibilityChangeEvent = null;
hiddenProperty = null;
document.removeEventListener(visibilityChangeEvent, shoutboxOnVisibilityChange)

// Enable
if(typeof ping_all === "undefined"){
    window.ping_all = true;
    window.beep = new Audio(cdn + "/js/sounds/beep.wav");
    $('<span id="notif_state" style="width: 10px; height: 10px; border-radius: 100%; display: inline-block; position: relative; top: 2px; left: 5px; background: #060;"></span>').insertAfter($('#fpShout .blockHeadCon b'))

    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    window.obs = new MutationObserver(function(mutations, observer) {
        if(window.ping_all){
            // look through all mutations that just occured
            for(var i=0; i<mutations.length; ++i) {
                // look through all added nodes of this mutation
                for(var j=0; j<mutations[i].addedNodes.length; ++j) {
                    // was a child added with ID of 'bar'?
                    if(mutations[i].addedNodes[j].id.indexOf('sbid') > -1) {
                        // shoutboxAudios['beep'].pause()
                        // shoutboxAudios['beep'].currentTime = 0
                        window.beep.play()   
                    }
                }
            }
        }
    });
    window.obs.observe($("#shoutbox>div").get(0), {
        childList: true
    });
}else{
    window.ping_all = true;
    $("#notif_state").css("background-color", "#060")
}
console.info("[NOTIFICATIONS]: activated");